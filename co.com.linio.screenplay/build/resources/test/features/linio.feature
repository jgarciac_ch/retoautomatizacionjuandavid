#Author: juandavidgarcia@outlook.com
@tag
Feature: Reto Linio
  Como usuario de Linio
  quiero hacer una busqueda
  y agregar un producto al carrito de compras.

  @linio
  Scenario: Agregar un producto al carrito de compras
    Given que Juan quiere entrar a la pagina de Linio
    When el busca un producto
    And el agrega un producto al carrito de compras
    Then el valida el producto en el carrito de compras