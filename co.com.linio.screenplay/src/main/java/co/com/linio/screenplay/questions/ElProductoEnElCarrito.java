package co.com.linio.screenplay.questions;

import co.com.linio.screenplay.ui.CarritoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElProductoEnElCarrito implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {

		return Text.of(CarritoPage.PRODUCTO_NOMBRE).viewedBy(actor).asString();
	}

	public static ElProductoEnElCarrito es() {
		return new ElProductoEnElCarrito();
	}
	
	

}
