package co.com.linio.screenplay.tasks;

import static co.com.linio.screenplay.exceptions.ElementoNoCarga.ELEMENTO_NO_CARGA;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import co.com.linio.screenplay.exceptions.ElementoNoCarga;
import co.com.linio.screenplay.ui.LinioHomePage;
import co.com.linio.screenplay.ui.LinioResultsPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class Buscar implements Task {
	
	private String producto;
	
	public Buscar(String producto) {
		super();
		this.producto = producto;
	}
	
	@Override
	@Step ("{1} Buscar el producto")
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(LinioHomePage.CAMPO_DE_BUSQUEDA));
		actor.attemptsTo(Enter.theValue(producto).into(LinioHomePage.CAMPO_DE_BUSQUEDA));
		actor.attemptsTo(Click.on(LinioHomePage.BOTON_BUSCAR));
		actor.should(GivenWhenThen.seeThat(the(LinioResultsPage.ENCABEZADO_TELEVISORES), isVisible()).orComplainWith(ElementoNoCarga.class, ELEMENTO_NO_CARGA));
		
	}

	public static Buscar ElProducto(String producto) {
	
		return Tasks.instrumented(Buscar.class, producto);
	}
	

}
