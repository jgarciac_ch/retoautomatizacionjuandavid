package co.com.linio.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://www.linio.com.co/")
public class LinioHomePage extends PageObject{
	
	public static final Target TITULO = Target.the("El titulo de la p�gina Linio").located(By.xpath("//*[@id=\"navbar\"]/div[1]/div[2]/div[1]/a"));
	public static final Target CAMPO_DE_BUSQUEDA = Target.the("El campo donde se ingresa el producto a buscar").located(By.name("q"));
	public static final Target BOTON_BUSCAR = Target.the("El bot�n buscar").located(By.xpath("//*[@id=\"navbar\"]/div[1]/div[2]/div[2]/form/div/div[3]/button"));
	
}
