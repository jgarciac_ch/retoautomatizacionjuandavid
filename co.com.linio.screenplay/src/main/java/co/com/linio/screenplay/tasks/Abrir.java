package co.com.linio.screenplay.tasks;

import static co.com.linio.screenplay.exceptions.PaginaNoIniciaException.PAGINA_NO_CARGA;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import co.com.linio.screenplay.exceptions.PaginaNoIniciaException;
import co.com.linio.screenplay.ui.LinioHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;


public class Abrir implements Task {
	
	private LinioHomePage linioHomePage;
	
	@Override
	@Step ("{0} Abre la p�gina Linio.com.co")
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(linioHomePage));
		actor.should(GivenWhenThen.seeThat(the(LinioHomePage.TITULO), isVisible()).orComplainWith(PaginaNoIniciaException.class, PAGINA_NO_CARGA));
		
	}

	public static Abrir LaPaginaDeLinio() {
		return Tasks.instrumented(Abrir.class);
	}

	

}
