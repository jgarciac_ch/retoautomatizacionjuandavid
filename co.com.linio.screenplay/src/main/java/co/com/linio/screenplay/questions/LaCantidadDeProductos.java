package co.com.linio.screenplay.questions;

import co.com.linio.screenplay.ui.CarritoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaCantidadDeProductos implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(CarritoPage.PRODUCTO_CANTIDAD).viewedBy(actor).asString();
	}

	public static LaCantidadDeProductos es() {
	
		return new LaCantidadDeProductos();
	}
	
	
	

}
