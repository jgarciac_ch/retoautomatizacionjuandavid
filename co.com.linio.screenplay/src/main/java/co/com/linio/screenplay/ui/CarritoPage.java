package co.com.linio.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class CarritoPage extends PageObject{
	
	public static final Target PRODUCTO_NOMBRE = Target.the("Nombre del producto").located(By.xpath("//div[@class='item-title col-md-12 col-sm-11 col-xs-10']/span/a"));
	public static final Target PRODUCTO_CANTIDAD = Target.the("Cantidad del producto").located(By.xpath("/html/body/div[3]/main/div[1]/div/div[1]/div[2]/div[2]/div[5]/select/option[1]"));
	

}
