package co.com.linio.screenplay.exceptions;

public class ElementoNoCarga extends AssertionError{
	
	private static final long serialVersionUID = 1L;
	public static final String ELEMENTO_NO_CARGA = "Los Elementos de la p�gina no cargaron correctamente.";

	public ElementoNoCarga(String message, Throwable cause) {
		super(message, cause);

	}

}
