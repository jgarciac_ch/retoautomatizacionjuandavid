package co.com.linio.screenplay.exceptions;

public class PaginaNoIniciaException extends AssertionError{
	
	private static final long serialVersionUID = 1L;
	public static final String PAGINA_NO_CARGA = "La p�gina de inicio no carga";

	public PaginaNoIniciaException(String message, Throwable cause) {
		super(message, cause);

	}

}
