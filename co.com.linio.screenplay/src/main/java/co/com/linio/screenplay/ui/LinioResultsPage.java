package co.com.linio.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LinioResultsPage extends PageObject {
	
	public static final Target PRODUCTO = Target.the("Producto a Seleccionar").located(By.xpath("//span[contains(text(),'49UK6200')]"));
	public static final Target BOTON_A�ADIR_AL_CARRITO = Target.the("Boton A�adir al carrito").located(By.id("buy-now"));
	public static final Target BOTON_IR_AL_CARRITO = Target.the("Boton Ir al Carrito").located(By.xpath("//a[@class=\"btn btn-sm btn-go-to-cart\"]"));
	public static final Target ENCABEZADO_TELEVISORES = Target.the("Encabezado Televisores").located(By.xpath("//*[@id=\"catalogue-list-container\"]/h1"));
	

}
