package co.com.linio.screenplay.tasks;

import co.com.linio.screenplay.ui.LinioResultsPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;
import net.thucydides.core.annotations.Step;


public class AgregarProducto implements Task {

	@Override
	@Step ("{2} Agrega el producto al carrito")
	public <T extends Actor> void performAs(T actor) {
		
		//actor.should(GivenWhenThen.seeThat(the(LinioHomePage.TITULO), isVisible()).orComplainWith(PaginaNoIniciaException.class, PAGINA_NO_CARGA));
		actor.attemptsTo(Click.on(LinioResultsPage.PRODUCTO));
		actor.remember("producto",Text.of(LinioResultsPage.PRODUCTO).viewedBy(actor).asString());
		actor.attemptsTo(Click.on(LinioResultsPage.BOTON_A�ADIR_AL_CARRITO));
        actor.attemptsTo(Click.on(LinioResultsPage.BOTON_IR_AL_CARRITO));
       	
	}

	public static AgregarProducto AlCarrito() {
		return Tasks.instrumented(AgregarProducto.class);
	}
	
	
	

}
