package co.com.linio.screenplay.stepdefinitions;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.linio.screenplay.questions.ElProductoEnElCarrito;
import co.com.linio.screenplay.questions.LaCantidadDeProductos;
import co.com.linio.screenplay.tasks.Abrir;
import co.com.linio.screenplay.tasks.AgregarProducto;
import co.com.linio.screenplay.tasks.Buscar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class linioStepDefinition {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial() {
		juan.can(BrowseTheWeb.with(hisBrowser));	
	}
	
	@Given("^que Juan quiere entrar a la pagina de Linio$")
	public void queJuanQuiereEntrarALaPaginaDeLinio() throws Exception {
		
		juan.wasAbleTo(Abrir.LaPaginaDeLinio());
	}
	
	@When("^el busca un producto$")
	public void elBuscaUnProducto() throws Exception {
		
		juan.attemptsTo(Buscar.ElProducto("Televisor"));

	}

	@When("^el agrega un producto al carrito de compras$")
	public void elAgregaUnProductoAlCarritoDeCompras() throws Exception {
		
		juan.attemptsTo(AgregarProducto.AlCarrito());

	}

	@Then("^el valida el producto en el carrito de compras$")
	public void elValidaElProductoEnElCarritoDeCompras() throws Exception {
		
		juan.should(seeThat(ElProductoEnElCarrito.es(), Matchers.equalTo(juan.recall("producto").toString())));
		juan.should(seeThat(LaCantidadDeProductos.es(), Matchers.equalTo("1")));
	}

}
